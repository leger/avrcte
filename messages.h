#include "common.h"

#ifndef MESSAGES_H
#define MESSAGES_H 1

#include <util/crc16.h>

uint8_t get_packet(char * z, uint8_t pid);
void message_send();
void message_get(const char* msg);
void readed_packet(const char * z, uint8_t len, config_t * nc);
void update_temp_readed_status_packet(const char *z, uint8_t len);

/*[[[cog
import conf
hbid_water = f"{conf.BID_WATER:02x}"
cog.out(f"#define BID_WATER_C0 '{hbid_water[0]}'\n")
cog.out(f"#define BID_WATER_C1 '{hbid_water[1]}'\n")
]]] */
#define BID_WATER_C0 'c'
#define BID_WATER_C1 '0'
//[[[end]]]

#endif
