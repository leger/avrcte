#!/usr/bin/python3

import os
import time
import subprocess
import re
import sys

CMDCOMMIT = ('git', 'describe', '--abbr=7', '--dirty', '--long')

def retain_out(x):
    return subprocess.check_output(x).strip().decode('utf8')

if __name__ == '__main__':
    commit = retain_out(CMDCOMMIT)
    a = re.match("v([0-9]+)\.([0-9]+)-([0-9]+)-g([0-9a-f]{7})((?:-dirty)?)$",commit)
    if a:
        major, minor, nbc, commithash, dirty = a.groups()
        dirtyhash = (int(commithash,16)<<4) + (1 if dirty else 0)
        print(f'#define COMMITID_MAJOR {major}')
        print(f'#define COMMITID_MINOR {minor}')
        print(f'#define COMMITID_NBCOMMITS 0x{int(nbc):02x}')
        print(f'#define COMMITID_DIRTYHASH 0x{dirtyhash:08x}')
    else:
        sys.exit(1)
