#!/usr/bin/python3

import re

with open("ID") as f:
    content = f.read().strip()
a = re.match("^0x([0-9a-fA-F]{2})$", content)
if not a:
    raise SyntaxError

(hbid,) = a.groups()

bid = int(hbid, 16)
hbid = f"{bid:02x}"
print(f"#define BID 0x{hbid}")
print(f"#define BID_C0 '{hbid[0]}'")
print(f"#define BID_C1 '{hbid[1]}'")
