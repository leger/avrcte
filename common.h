#ifndef _H_COMMON
#define _H_COMMON 1

#define F_CPU 32000000L

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

#include "utils.h"
#include "commitid.h"
#include "id.h"
#include "clock.h"
#include "debug.h"
#include "bus.h"
#include "config.h"
#include "status.h"
#include "messages.h"
#include "vanne.h"
#include "orders.h"
#include "debit.h"
#include "thermometer.h"
#include "median_filter.h"
#include "leds.h"
#include "main_timer.h"

#endif
