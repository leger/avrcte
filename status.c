#include "status.h"

status_t status;

void status_init()
{
    status.temperature.v1 = 0;
    status.temperature.v2 = 0;
    status.debit60.v1 = 0;
    status.debit60.v2 = 0;
    status.debit5.v1 = 0;
    status.debit5.v2 = 0;
    status.volume_manque = 0;
    status.accu_integrateur = 0;
    status.fail = 0;
    status.temperature_cold_water = 20;
    status.temperature_hot_water = 20;
    status.consecutive_closed_debit = 0;
    status.force_closed = 0;
}
