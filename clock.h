#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.clock32M(header=True)
)
]]]*/
#ifndef H_clock32M
#define H_clock32M 1

#include <avr/io.h>

void init_osc();

#endif
///[[[end]]]
