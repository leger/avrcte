#include "common.h"

#ifndef STATUS_H
#define STATUS_H 1

#define STATUS_FAIL_T (0x01)
#define STATUS_FAIL_UNDERFLOW (0x01<<1)
#define STATUS_FAIL_OVERFLOW (0x01<<2)

typedef struct status_struct
{
    int16_t temperature_insta;
    filtre2_t temperature;
    filtre2_t debit5;
    filtre2_t debit60;
    float volume_manque;
    float consigne_debit;
    float consigne_adj;
    float accu_integrateur;
    uint8_t fail;
    uint16_t fail_get_T;
    float temperature_cold_water;
    float temperature_hot_water;
    uint16_t consecutive_closed_debit;
    uint8_t force_closed;
} status_t;

extern status_t status;

void status_init();

#endif
