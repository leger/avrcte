
#include "debit.h"

void debit_init()
{
    FLOW_PORT.DIRCLR = FLOW_PIN_bm;
    FLOW_PINCTRL = PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;
    FLOW_EVSYS_CHANNELMUX = FLOW_EVSYS_CHMUX;
    FLOW_EVSYS_CHANNELCTRL = EVSYS_DIGFILT_2SAMPLES_gc;

    FLOW_COUNTER.PER=0xffff;
    FLOW_COUNTER.CNT = 0;
    FLOW_COUNTER.CTRLA = (FLOW_COUNTER.CTRLA & ~TC0_CLKSEL_gm) | FLOW_COUNTER_EVENT;

    status.debit5.v1 = 0;
    status.debit5.v2 = 0;
    status.debit60.v1 = 0;
    status.debit60.v2 = 0;
    status.volume_manque = 0;
    status.consigne_debit = 0;
    status.consigne_adj = 0;
}

void debit_update()
{
    uint16_t count = FLOW_COUNTER.CNT;
    FLOW_COUNTER.CNT = 0;
    float vol_sur_seconde = 1.8500e-04*count+(2.9027e-06*count)*count;
    float debit_insta = vol_sur_seconde*60;
    status.volume_manque += status.consigne_debit/60-vol_sur_seconde;
    status.fail &= ~(STATUS_FAIL_UNDERFLOW|STATUS_FAIL_OVERFLOW);
    if(status.volume_manque > cconfig.volume_manque_max)
    {
        status.volume_manque = cconfig.volume_manque_max;
        status.fail |= STATUS_FAIL_UNDERFLOW;
    }
    else if(status.volume_manque < - cconfig.volume_manque_max)
    {
        status.volume_manque = - cconfig.volume_manque_max;
        status.fail |= STATUS_FAIL_OVERFLOW;
    }

    update_filtre2(&(status.debit5), FLOW_FACTOR_FILTRE_5, debit_insta);
    update_filtre2(&(status.debit60), FLOW_FACTOR_FILTRE_60, debit_insta);

    status.consigne_adj = status.consigne_debit + status.volume_manque/cconfig.m_rattrapage;
    if(status.consigne_adj<0)
        status.consigne_adj = 0;
    
    if(vanne_internal_status == EV_CLOSED)
    {
        if(count==0)
        {
            status.consecutive_closed_debit = 0;
        }
        else
        {
            if(status.consecutive_closed_debit == 5)
            {
                orders.force_close_ev = 1;
                status.consecutive_closed_debit = 0;
            }
            else
                status.consecutive_closed_debit++;
        }
    }
    else
        status.consecutive_closed_debit = 0;
}

void debit_vanne_update()
{ 
    if(cconfig.mode == MODE_AUTO || cconfig.mode == MODE_DEBIT || cconfig.mode == MODE_POST_REFROI)
    {
        if( (status.consigne_adj>cconfig.debit_min2) ||
                ((status.consigne_adj>cconfig.debit_min1) && (vanne_internal_status != EV_CLOSED)))
        {
            float erreur = status.consigne_adj - status.debit5.v2;
            if(erreur>0)
            {
                // open step
                orders.open_ev_step_value = cconfig.cst_ev * erreur;
                orders.open_ev_step = 1;
            }
            else
            {
                // close step
                orders.close_ev_step_value = cconfig.cst_ev * (-erreur);
                orders.close_ev_step = 1;
            }
        }
        else
        {
            // close ev
            orders.close_ev = 1;
        }
    }
}
