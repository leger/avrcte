#include "common.h"

#ifndef CONFIG_H
#define CONFIG_H 1

/*[[[cog
import avrxmegastuff
import conf
cog.out(f"#define EEPROM_ADDR_CONFIG {conf.EEPROM_ADDR_CONFIG}\n")
]]]*/
#define EEPROM_ADDR_CONFIG ((uint8_t*)0x0000)
//[[[end]]]

#include <avr/eeprom.h>

typedef enum mode_enum
{
    MODE_FORCE_CLOSE = 0x01,
    MODE_FORCE_OPEN = 0x02,
    MODE_FORCE_VIDANGE = 0x03,
    MODE_DEBIT = 0x04,
    MODE_AUTO = 0x05,
    MODE_POST_REFROI = 0x06,
} mode_t;

typedef struct config_struct
{
    uint8_t mode;
    uint16_t vol_cuve;
    uint16_t Kp;
    uint16_t ti;
    int16_t consigne_temperature;
    uint16_t debit_max;
    uint8_t cst_ev;
    uint16_t debit_min1;
    uint16_t debit_min2;
    uint8_t minimum_step;
    uint16_t volume_manque_max;
    uint8_t m_rattrapage;
    uint16_t consigne_debit;
    int16_t post_refroi_T1;
    int16_t post_refroi_T2;
    int16_t post_refroi_cold_water_T1;
    int16_t post_refroi_cold_water_T2;
    int16_t post_refroi_hot_water_T1;
    int16_t post_refroi_hot_water_T2;
    uint16_t post_refroi_dmax;
} config_t;

typedef struct cconfig_struct
{
    mode_t mode;
    float Kp;
    float ti;
    float consigne_temperature;
    float debit_max;
    float cst_ev;
    float debit_min1;
    float debit_min2;
    uint16_t minimum_step;
    float debit_no_action;
    float volume_manque_max;
    float m_rattrapage;
    float consigne_debit;
    float post_refroi_T1;
    float post_refroi_T2;
    float post_refroi_cold_water_T1;
    float post_refroi_cold_water_T2;
    float post_refroi_hot_water_T1;
    float post_refroi_hot_water_T2;
    float post_refroi_dmax;
} cconfig_t;

void config_read();
void config_write();
void cconfig_update(config_t *, cconfig_t *);
void config_init();
uint8_t config_valid(cconfig_t *);
void config_default(config_t *);
void config_apply_new_config(config_t *);
void config_mode_change();

extern config_t config;
extern cconfig_t cconfig;

#endif
