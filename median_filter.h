#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.median_filter(
        prefix="median_filter_thermometer",
        filter_len=conf.THERMO_MEDIAN_FILTER_LEN,
        data_type="int16_t",
        idx_type="uint8_t",
        header=True,
    )
)
]]]*/
#ifndef H_MF_median_filter_thermometer
#define H_MF_median_filter_thermometer 1

#define MEDIAN_FILTER_THERMOMETER_len 13

void median_filter_thermometer_fill(int16_t);
void median_filter_thermometer_update(int16_t);
int16_t median_filter_thermometer_read();

extern volatile uint8_t median_filter_thermometer_filled;
#endif
//[[[end]]]
