#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.ds18b20(
        prefix="thermometer",
        pindesc=conf.SENSOR_T,
        header=True,
    )
)
]]]*/
#ifndef H_DS18B20_thermometer
#define H_DS18B20_thermometer 1

#define THERMOMETER_port PORTE
#define THERMOMETER_pin PIN5_bm
#define THERMOMETER_CMD_CONVERTTEMP 0x44
#define THERMOMETER_CMD_RSCRATCHPAD 0xbe
#define THERMOMETER_CMD_WSCRATCHPAD 0x4e
#define THERMOMETER_CMD_CPYSCRATCHPAD 0x48
#define THERMOMETER_CMD_RECEEPROM 0xb8
#define THERMOMETER_CMD_RPWRSUPPLY 0xb4
#define THERMOMETER_CMD_SEARCHROM 0xf0
#define THERMOMETER_CMD_READROM 0x33
#define THERMOMETER_CMD_MATCHROM 0x55
#define THERMOMETER_CMD_SKIPROM 0xcc
#define THERMOMETER_CMD_ALARMSEARCH 0xec

#include <util/delay.h>
#include <util/crc16.h>

uint8_t thermometer_reset();
void thermometer_writebit(uint8_t bit);
uint8_t thermometer_readbit(void);
void thermometer_writebyte(uint8_t byte);
uint8_t thermometer_readbyte(void);
int8_t thermometer_gettemp(int16_t* temp, uint16_t timeout_ms);
#endif
///[[[end]]]
