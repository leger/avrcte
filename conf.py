import collections
import math

from avrxmegastuff import PinDesc

VANNE = {
    "cmd_open_close_pindesc": (PinDesc("E0"), PinDesc("E1")),
    "sensor_open_close_pindesc": (PinDesc("E3"), PinDesc("E4")),
    "action_limit": "cconfig.minimum_step",
}

FLOW = PinDesc("E2")
FLOW_EVSYS_CHANNEL = 1
FLOW_COUNTER = "TCD0"
FLOW_FACTOR_FILTRE_5 = 1 - math.exp(-1 / 5)
FLOW_FACTOR_FILTRE_60 = 1 - math.exp(-1 / 60)

SENSOR_T = PinDesc("E5")

LED = {
    "STATUS": PinDesc("F0"),
    "TX": PinDesc("F1"),
    "RX": PinDesc("F2"),
    "MOTOR_1": PinDesc("F3"),
    "MOTOR_2": PinDesc("F4"),
    "OVERFLOW": PinDesc("F5"),
    "UNDERFLOW": PinDesc("F6"),
}

DEBUG = {"name": "C1", "txpin": 7, "tx": True}

BUS = {
    "name": "C0",
    "txpin": 3,
    "tx": True,
    "rx": True,
    "de_pindesc": PinDesc("C0"),
    "rxhandler": "bus_msg_handler",
    "txlen": 384,
    "rxlen": 384,
    "baudrate": 1200,
    "rxtriggerbegin": ("S", "M"),
}

THERMO_PERIOD = 5
THERMO_MEDIAN_FILTER_LEN = 13
THERMO_FACTOR_FILTRE = 1 - math.exp(-5 / 60)
THERMO_FAIL_NUMBER = 180
BID_WATER = 0xc0

ACTION_TIMER_MAIN = "TCC0"

EEPROM_ADDR_CONFIG = "((uint8_t*)0x0000)"
