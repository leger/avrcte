#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.median_filter(
        prefix="median_filter_thermometer",
        filter_len=conf.THERMO_MEDIAN_FILTER_LEN,
        data_type="int16_t",
        idx_type="uint8_t",
        header=False,
    )
)
]]]*/
volatile int16_t median_filter_thermometer_circbuf[MEDIAN_FILTER_THERMOMETER_len];
volatile uint8_t median_filter_thermometer_circidx;
volatile uint8_t median_filter_thermometer_filled;

void median_filter_thermometer_fill(int16_t value)
{
    for(median_filter_thermometer_circidx = 0; median_filter_thermometer_circidx<MEDIAN_FILTER_THERMOMETER_len; median_filter_thermometer_circidx++)
        median_filter_thermometer_circbuf[median_filter_thermometer_circidx] = value;
    median_filter_thermometer_circidx=0;
    median_filter_thermometer_filled = 1;
}

void median_filter_thermometer_update(int16_t value)
{
    median_filter_thermometer_circbuf[median_filter_thermometer_circidx] = value;
    median_filter_thermometer_circidx++;
    median_filter_thermometer_circidx%=MEDIAN_FILTER_THERMOMETER_len;
}

int16_t median_filter_thermometer_read()
{
    int16_t temp[MEDIAN_FILTER_THERMOMETER_len];

    uint8_t i;
    for(i=0; i<MEDIAN_FILTER_THERMOMETER_len; i++)
        temp[i] = median_filter_thermometer_circbuf[i];

    uint8_t j;

    for(i=0; i<=MEDIAN_FILTER_THERMOMETER_len/2; i++)
    {
        for(j=i+1; j<MEDIAN_FILTER_THERMOMETER_len; j++)
        {
            if(temp[i]>temp[j])
            {
                int16_t swap = temp[i];
                temp[i] = temp[j];
                temp[j] = swap;
            }
        }
    }

    return temp[MEDIAN_FILTER_THERMOMETER_len/2];
}
//[[[end]]]
