#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.uart(
        prefix="debug",
        **conf.DEBUG,
        header=False,
    )
)
]]]*/
    volatile uint8_t debug_txbuffer[DEBUG_txlen];
    volatile uint16_t debug_txidxB;
    volatile uint16_t debug_txidxA;


void debug_init()
{
    DEBUG_port.OUTSET = DEBUG_txpin;
    DEBUG_port.DIRSET = DEBUG_txpin;

    uint16_t bscale = 0;
    if(DEBUG_baudrate<600)
        bscale++;
    if(DEBUG_baudrate<300)
        bscale++;
    if(DEBUG_baudrate<150)
        bscale++;
    if(DEBUG_baudrate<75)
        bscale++;
    if(DEBUG_baudrate<36)
        bscale++;
    if(DEBUG_baudrate<18)
        bscale++;
    if(DEBUG_baudrate<9)
        bscale++;

    uint16_t bsel = (F_CPU-((((uint32_t)8)<<bscale) * DEBUG_baudrate)) / ((((uint32_t)16)<<bscale) * DEBUG_baudrate);

    DEBUG_USART.BAUDCTRLA = bsel;
    DEBUG_USART.BAUDCTRLB = (bscale << USART_BSCALE_gp) | (bsel >> 8);

    DEBUG_USART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc;
    if(DEBUG_chsize==5)
        DEBUG_USART.CTRLC |= USART_CHSIZE_5BIT_gc;
    else if(DEBUG_chsize==6)
        DEBUG_USART.CTRLC |= USART_CHSIZE_6BIT_gc;
    else if(DEBUG_chsize==7)
        DEBUG_USART.CTRLC |= USART_CHSIZE_7BIT_gc;
    else if(DEBUG_chsize==8)
        DEBUG_USART.CTRLC |= USART_CHSIZE_8BIT_gc;
    else
        DEBUG_USART.CTRLC |= USART_CHSIZE_9BIT_gc;
    DEBUG_USART.CTRLA = (DEBUG_rxen?USART_RXCINTLVL_MED_gc:USART_RXCINTLVL_OFF_gc);
    DEBUG_USART.CTRLB = (DEBUG_rxen?USART_RXEN_bm:0x00) | (DEBUG_txen?USART_TXEN_bm:0x00);
}

    
ISR(DEBUG_DRE_vect)
{
    if(debug_txidxA == debug_txidxB)
    {
        // empty
        DEBUG_USART.CTRLA = (DEBUG_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_OFF_gc;
    }
    else
    {
        DEBUG_USART.DATA = debug_txbuffer[debug_txidxA];
        debug_txidxA++;
        debug_txidxA%=DEBUG_txlen;
    }
}

void debug_putchar(uint8_t ch)
{
    while((debug_txidxB+1)%DEBUG_txlen == debug_txidxA);

    debug_txbuffer[debug_txidxB] = ch;
    debug_txidxB++;
    debug_txidxB%=DEBUG_txlen;

    DEBUG_USART.CTRLA = (DEBUG_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_MED_gc;
}

void debug_putzchar(const char* zchar)
{
    uint16_t i=0;
    while(zchar[i]!=0)
    {
        debug_putchar(zchar[i]);
        i++;
    }
}

void debug_putfloat(const float f, uint8_t decsize)
{
    int32_t fl;

    char buf[64];
    
    if(decsize>0)
    {
        fl=f;
        float decimal = f-fl;
        if(decimal<0)
            decimal*=-1;
        
        char bufformat[64];
        uint8_t i;
        for(i=0;i<decsize;i++)
            decimal*=10;
        int16_t n;
        n = snprintf(bufformat,64,"%%li.%%0%ulu",decsize);
        if(n<64)
        {
            n = snprintf(buf,64,bufformat,fl,(uint32_t)(decimal));
            if(n<63)
                debug_putzchar(buf);
        }
    }
    else
    {
        if(f>=0)
            fl = f+.5;
        else
            fl = f-.5;

        int16_t n = snprintf(buf,64,"%li.",fl);
        if(n<63)
            debug_putzchar(buf);
    }
}

void debug_printf(const char *fmt, ...)
{
    char buf[DEBUG_txlen];

    va_list ap;
    va_start(ap, fmt);
    int16_t n = vsnprintf(buf, DEBUG_txlen, fmt, ap);
    va_end(ap);

    if (n < DEBUG_txlen)
        debug_putzchar(buf);
}
///[[[end]]]
