#include "common.h"

#ifndef H_ACTION
#define H_ACTION 1

/*[[[cog
import conf
import avrxmegastuff

cog.out(f"#define ACTION_TIMER_MAIN {conf.ACTION_TIMER_MAIN}\n")
cog.out(f"#define ACTION_TIMER_MAIN_OVF_vect {conf.ACTION_TIMER_MAIN}_OVF_vect\n")

]]] */
#define ACTION_TIMER_MAIN TCC0
#define ACTION_TIMER_MAIN_OVF_vect TCC0_OVF_vect
// [[[end]]]

void main_timer_init();



#endif
