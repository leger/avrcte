#include "utils.h"

void update_filtre2(filtre2_t* pf, float factor, float value)
{
    pf->v1 += factor*(value - pf->v1);
    pf->v2 += factor*(pf->v1 - pf->v2);
}

char snip_to_char(uint8_t snip)
{
    if(snip<10)
        return snip+'0';
    return snip-10+'a';
}

uint8_t snip_from_char(char c)
{
    if(c<='9' && c>='0')
        return c-'0';
    if(c<='F' && c>='A')
        return 0x0A+(c-'A');
    if(c<='f' && c>='a')
        return 0x0A+(c-'a');
    return 0;
}

void uint8_to_2char(uint16_t nb, char* c)
{
    c[0] = snip_to_char((nb>>4) & 0x0F);
    c[1] = snip_to_char( nb     & 0x0F);
}

void uint16_to_4char(uint16_t nb, char* c)
{
    c[0] = snip_to_char((nb>>12) & 0x0F);
    c[1] = snip_to_char((nb>>8)  & 0x0F);
    c[2] = snip_to_char((nb>>4)  & 0x0F);
    c[3] = snip_to_char( nb      & 0x0F);
}

void uint32_to_8char(uint32_t nb, char* c)
{
    c[0] = snip_to_char((nb>>28) & 0x0F);
    c[1] = snip_to_char((nb>>24) & 0x0F);
    c[2] = snip_to_char((nb>>20) & 0x0F);
    c[3] = snip_to_char((nb>>16) & 0x0F);
    c[4] = snip_to_char((nb>>12) & 0x0F);
    c[5] = snip_to_char((nb>>8)  & 0x0F);
    c[6] = snip_to_char((nb>>4)  & 0x0F);
    c[7] = snip_to_char( nb      & 0x0F);
}

uint8_t uint8_from_2char(const char* c)
{
    uint8_t nb = 0x00;
    nb += snip_from_char(c[0]);
    nb<<=4;
    nb += snip_from_char(c[1]);
    return nb;
}

uint16_t uint16_from_4char(const char* c)
{
    uint16_t nb = 0x00;
    nb += snip_from_char(c[0]);
    nb<<=4;
    nb += snip_from_char(c[1]);
    nb<<=4;
    nb += snip_from_char(c[2]);
    nb<<=4;
    nb += snip_from_char(c[3]);
    return nb;
}

uint32_t uint32_from_8char(const char* c)
{
    uint32_t nb = 0x00;
    nb += snip_from_char(c[0]);
    nb<<=4;
    nb += snip_from_char(c[1]);
    nb<<=4;
    nb += snip_from_char(c[2]);
    nb<<=4;
    nb += snip_from_char(c[3]);
    nb<<=4;
    nb += snip_from_char(c[4]);
    nb<<=4;
    nb += snip_from_char(c[5]);
    nb<<=4;
    nb += snip_from_char(c[6]);
    nb<<=4;
    nb += snip_from_char(c[7]);
    return nb;
}
