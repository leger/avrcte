DEVICE     = atxmega256a3u
PROGRAMMER = -c avrispmkII
OBJECTS    = main.o clock.o debug.o bus.o config.o status.o utils.o messages.o vanne.o debit.o orders.o thermometer.o median_filter.o leds.o main_timer.o
PORT		= usb

# For computing fuse byte values for other devices and options see
# the fuse bit calculator at http://www.engbedded.com/fusecalc/


# Tune the lines below only if you know what you are doing:

LFLAGS =
#LFLAGS = -Wl,-u,vfprintf -lprintf_flt
AVRDUDE = avrdude -v -v $(PROGRAMMER) -p $(DEVICE)  -P $(PORT) 
COMPILE = avr-gcc -Wall -Os -mmcu=$(DEVICE)

.PHONY: clean all

.INTERMEDIATE: commitid.h id.h message.o main.o

# symbolic targets:
all:	main.hex

clean:
		rm -f *.o *.elf *.hex commitid.h id.h

commitid.h: .git/refs/heads
	cog -r *.c *.h
	./define_commit_id.py > $@

id.h: ID
	./define_id.py > $@

%.o: %.c common.h commitid.h id.h
		$(COMPILE) -c $< -o $@

.S.o:
		$(COMPILE) -x assembler-with-cpp -c $< -o $@
		# "-x assembler-with-cpp" should not be necessary since this is the
		# default
		# file type for the .S (with capital S) extension. However, upper case
		# characters are not always preserved on Windows. To ensure WinAVR
		# compatibility define the file type manually.

flash:	all
		$(AVRDUDE) -B 60 -e -U flash:w:main.hex:i
		#	$(AVRDUDE) -U flash:w:main.hex:i

fuse:
		$(AVRDUDE) $(FUSES)

# file targets:
main.elf: $(OBJECTS)
		$(COMPILE) $(LFLAGS) -o main.elf $(OBJECTS)
		    
main.hex: main.elf
		rm -f main.hex
		avr-objcopy -j .text -j .data -O ihex main.elf main.hex
		avr-size --format=avr --mcu=$(DEVICE) main.elf #configOutput.elf
		# If you have an EEPROM section, you must also create a hex file
		# for the
		# EEPROM and add it to the "flash" target.
