#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.rs485(
        prefix="bus",
        **conf.BUS,
        header=False,
        rxnotify="led_alert.rx = 2;",
        txnotify="led_alert.tx = 2;",
    )
)
]]]*/
    volatile uint8_t bus_txbuffer[BUS_txlen];
    volatile uint16_t bus_txidxB;
    volatile uint16_t bus_txidxA;

    volatile uint8_t bus_rxbuffer[BUS_rxlen];
    volatile uint16_t bus_rxidx;

void bus_init()
{
    BUS_port.OUTSET = BUS_txpin;
    BUS_port.DIRSET = BUS_txpin;
    BUS_deport.DIRSET = BUS_depin;
    BUS_deport.OUTCLR = BUS_depin;

    uint16_t bscale = 0;
    if(BUS_baudrate<600)
        bscale++;
    if(BUS_baudrate<300)
        bscale++;
    if(BUS_baudrate<150)
        bscale++;
    if(BUS_baudrate<75)
        bscale++;
    if(BUS_baudrate<36)
        bscale++;
    if(BUS_baudrate<18)
        bscale++;
    if(BUS_baudrate<9)
        bscale++;

    uint16_t bsel = (F_CPU-((((uint32_t)8)<<bscale) * BUS_baudrate)) / ((((uint32_t)16)<<bscale) * BUS_baudrate);

    BUS_USART.BAUDCTRLA = bsel;
    BUS_USART.BAUDCTRLB = (bscale << USART_BSCALE_gp) | (bsel >> 8);

    BUS_USART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc;
    if(BUS_chsize==5)
        BUS_USART.CTRLC |= USART_CHSIZE_5BIT_gc;
    else if(BUS_chsize==6)
        BUS_USART.CTRLC |= USART_CHSIZE_6BIT_gc;
    else if(BUS_chsize==7)
        BUS_USART.CTRLC |= USART_CHSIZE_7BIT_gc;
    else if(BUS_chsize==8)
        BUS_USART.CTRLC |= USART_CHSIZE_8BIT_gc;
    else
        BUS_USART.CTRLC |= USART_CHSIZE_9BIT_gc;
    BUS_USART.CTRLA = (BUS_rxen?USART_RXCINTLVL_MED_gc:USART_RXCINTLVL_OFF_gc)|(BUS_txen?USART_TXCINTLVL_MED_gc:0x00);
    BUS_USART.CTRLB = (BUS_rxen?USART_RXEN_bm:0x00) | (BUS_txen?USART_TXEN_bm:0x00);
}

ISR(BUS_RXC_vect)
{
    uint8_t received = BUS_USART.DATA;
    led_alert.rx = 2;
    if(received==BUS_rxtrigger)
    {
        bus_rxbuffer[bus_rxidx]=0;
        bus_msg_handler((char*)bus_rxbuffer);
        bus_rxidx=0;
    }
    else
    {
        if(received == 'S' || received == 'M')
            bus_rxidx=0;
        bus_rxbuffer[bus_rxidx] = received;
        bus_rxidx++;
        if(bus_rxidx>=BUS_rxlen)
            bus_rxidx=0;
    }
}
    
ISR(BUS_DRE_vect)
{
    led_alert.tx = 2;
    BUS_deport.OUTSET = BUS_depin;
    if(bus_txidxA == bus_txidxB)
    {
        // empty
        BUS_USART.CTRLA = (BUS_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_OFF_gc;
    }
    else
    {
        BUS_USART.DATA = bus_txbuffer[bus_txidxA];
        bus_txidxA++;
        bus_txidxA%=BUS_txlen;
    }
}

ISR(BUS_TXC_vect)
  {
      BUS_deport.OUTCLR = BUS_depin;
  }

void bus_putchar(uint8_t ch)
{
    while((bus_txidxB+1)%BUS_txlen == bus_txidxA);

    bus_txbuffer[bus_txidxB] = ch;
    bus_txidxB++;
    bus_txidxB%=BUS_txlen;

    BUS_USART.CTRLA = (BUS_USART.CTRLA & ~USART_DREINTLVL_gm) | USART_DREINTLVL_MED_gc;
}

void bus_putzchar(const char* zchar)
{
    uint16_t i=0;
    while(zchar[i]!=0)
    {
        bus_putchar(zchar[i]);
        i++;
    }
}

void bus_putfloat(const float f, uint8_t decsize)
{
    int32_t fl;

    char buf[64];
    
    if(decsize>0)
    {
        fl=f;
        float decimal = f-fl;
        if(decimal<0)
            decimal*=-1;
        
        char bufformat[64];
        uint8_t i;
        for(i=0;i<decsize;i++)
            decimal*=10;
        int16_t n;
        n = snprintf(bufformat,64,"%%li.%%0%ulu",decsize);
        if(n<64)
        {
            n = snprintf(buf,64,bufformat,fl,(uint32_t)(decimal));
            if(n<63)
                bus_putzchar(buf);
        }
    }
    else
    {
        if(f>=0)
            fl = f+.5;
        else
            fl = f-.5;

        int16_t n = snprintf(buf,64,"%li.",fl);
        if(n<63)
            bus_putzchar(buf);
    }
}

void bus_printf(const char *fmt, ...)
{
    char buf[BUS_txlen];

    va_list ap;
    va_start(ap, fmt);
    int16_t n = vsnprintf(buf, BUS_txlen, fmt, ap);
    va_end(ap);

    if (n < BUS_txlen)
        bus_putzchar(buf);
}
///[[[end]]]

void bus_msg_handler(const char * msg)
{
    message_get(msg);
}
