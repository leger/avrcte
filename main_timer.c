#include "main_timer.h"

volatile uint8_t timer_each10;
volatile uint8_t timer_each50;
volatile uint8_t timer_each100;

void main_timer_init()
{
    ACTION_TIMER_MAIN.PER = 3125; // 10 Hz
    ACTION_TIMER_MAIN.CTRLA = ( ACTION_TIMER_MAIN.CTRLA & ~TC0_CLKSEL_gm ) | TC_CLKSEL_DIV1024_gc;
    ACTION_TIMER_MAIN.INTCTRLA = ( ACTION_TIMER_MAIN.INTCTRLA & ~TC0_OVFINTLVL_gm ) | TC_OVFINTLVL_MED_gc;
    timer_each10 = 1;
    timer_each50 = 23;
    timer_each100 = 48;
}

ISR(ACTION_TIMER_MAIN_OVF_vect)
{
    timer_each10++;
    if(timer_each10 == 10)
    {
        timer_each10=0;
        debit_update();
        led_toogle(LED_STATUS);
    }
    
    timer_each50++;
    if(timer_each50 == 50)
    {
        timer_each50=0;
        orders.get_T = 1;
    }

    timer_each100++;
    if(timer_each100 == 100)
    {
        timer_each100=0;
        debit_vanne_update();
    }

    led_update();
};

