#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.ev(
        prefix="vanne",
        **conf.VANNE,
        header=True,
    )
)
]]]*/
#ifndef H_EV_vanne
#define H_EV_vanne 1

#define VANNE_CMD_PORT PORTE
#define VANNE_CMD_OPEN_bm PIN0_bm
#define VANNE_CMD_CLOSE_bm PIN1_bm
#define VANNE_SENSOR_PORT PORTE
#define VANNE_SENSOR_OPEN_bm PIN3_bm
#define VANNE_SENSOR_CLOSE_bm PIN4_bm
#define VANNE_SENSOR_OPEN_PINCTRL PORTE.PIN3CTRL
#define VANNE_SENSOR_CLOSE_PINCTRL PORTE.PIN4CTRL
#define VANNE_ACTION_LIMIT cconfig.minimum_step

#ifndef H_ELECTROVANNE_TYPES
#define H_ELECTROVANNE_TYPES 1

typedef enum state_ev
{
    EV_MID,
    EV_OPENED,
    EV_CLOSED,
} ev_state_t;

#endif

extern volatile ev_state_t vanne_internal_status;

void vanne_init();
uint8_t vanne_internal_step_close(uint16_t d, uint8_t force);
uint8_t vanne_internal_step_open(uint16_t d, uint8_t force);
uint8_t vanne_step_close(uint16_t d, uint8_t force);
uint8_t vanne_step_open(uint16_t d, uint8_t force);
void vanne_close();
void vanne_force_close();
void vanne_open();
void vanne_vidange();
#endif
///[[[end]]]

void vanne_callback_open();
void vanne_callback_close();
