#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.rs485(
        prefix="bus",
        **conf.BUS,
        header=True,
    )
)
]]]*/
#ifndef H_UART_bus
#define H_UART_bus 1

#define BUS_port PORTC
#define BUS_txpin PIN3_bm
#define BUS_rxlen 384
#define BUS_txlen 384
#define BUS_baudrate 1200
#define BUS_chsize 8
#define BUS_rxtrigger '\n'
#define BUS_rxen true
#define BUS_txen true
#define BUS_USART USARTC0
#define BUS_DRE_vect USARTC0_DRE_vect
#define BUS_RXC_vect USARTC0_RXC_vect
#define BUS_TXC_vect USARTC0_TXC_vect
#define BUS_deport PORTC
#define BUS_depin PIN0_bm

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

void bus_init();

void bus_putchar(uint8_t ch);
void bus_putzchar(const char* zchar);
void bus_putfloat(const float f, uint8_t decsize);
void bus_printf(const char *fmt, ...);

void bus_msg_handler(const char *msg);
#endif
///[[[end]]]
