#include "leds.h"

/*[[[cog
import conf
import avrxmegastuff
]]] */
// [[[end]]]


/*[[[cog
cog.out("""
void led_init()
{
""")
for led_name in conf.LED.keys():
    cog.out(f"    LED_{led_name}_PORT.DIRSET = LED_{led_name}_PIN_bm;\n");
cog.out("""
}
""")
]]] */

void led_init()
{
    LED_STATUS_PORT.DIRSET = LED_STATUS_PIN_bm;
    LED_TX_PORT.DIRSET = LED_TX_PIN_bm;
    LED_RX_PORT.DIRSET = LED_RX_PIN_bm;
    LED_MOTOR_1_PORT.DIRSET = LED_MOTOR_1_PIN_bm;
    LED_MOTOR_2_PORT.DIRSET = LED_MOTOR_2_PIN_bm;
    LED_OVERFLOW_PORT.DIRSET = LED_OVERFLOW_PIN_bm;
    LED_UNDERFLOW_PORT.DIRSET = LED_UNDERFLOW_PIN_bm;

}
// [[[end]]]


/*[[[cog
for act, reg in (("on", "OUTSET"), ("off", "OUTCLR"), ("toogle", "OUTTGL")):
    cog.out(f"void led_{act}(led_t led)\n")
    cog.out("{\n")
    cog.out("    ")
    for led_name in conf.LED.keys():
        cog.out(f"if(led == LED_{led_name})\n");
        cog.out(f"        LED_{led_name}_PORT.{reg} = LED_{led_name}_PIN_bm;\n");
        cog.out(f"    else ");
    cog.out("{}\n");
    cog.out("}\n\n")
]]] */
void led_on(led_t led)
{
    if(led == LED_STATUS)
        LED_STATUS_PORT.OUTSET = LED_STATUS_PIN_bm;
    else if(led == LED_TX)
        LED_TX_PORT.OUTSET = LED_TX_PIN_bm;
    else if(led == LED_RX)
        LED_RX_PORT.OUTSET = LED_RX_PIN_bm;
    else if(led == LED_MOTOR_1)
        LED_MOTOR_1_PORT.OUTSET = LED_MOTOR_1_PIN_bm;
    else if(led == LED_MOTOR_2)
        LED_MOTOR_2_PORT.OUTSET = LED_MOTOR_2_PIN_bm;
    else if(led == LED_OVERFLOW)
        LED_OVERFLOW_PORT.OUTSET = LED_OVERFLOW_PIN_bm;
    else if(led == LED_UNDERFLOW)
        LED_UNDERFLOW_PORT.OUTSET = LED_UNDERFLOW_PIN_bm;
    else {}
}

void led_off(led_t led)
{
    if(led == LED_STATUS)
        LED_STATUS_PORT.OUTCLR = LED_STATUS_PIN_bm;
    else if(led == LED_TX)
        LED_TX_PORT.OUTCLR = LED_TX_PIN_bm;
    else if(led == LED_RX)
        LED_RX_PORT.OUTCLR = LED_RX_PIN_bm;
    else if(led == LED_MOTOR_1)
        LED_MOTOR_1_PORT.OUTCLR = LED_MOTOR_1_PIN_bm;
    else if(led == LED_MOTOR_2)
        LED_MOTOR_2_PORT.OUTCLR = LED_MOTOR_2_PIN_bm;
    else if(led == LED_OVERFLOW)
        LED_OVERFLOW_PORT.OUTCLR = LED_OVERFLOW_PIN_bm;
    else if(led == LED_UNDERFLOW)
        LED_UNDERFLOW_PORT.OUTCLR = LED_UNDERFLOW_PIN_bm;
    else {}
}

void led_toogle(led_t led)
{
    if(led == LED_STATUS)
        LED_STATUS_PORT.OUTTGL = LED_STATUS_PIN_bm;
    else if(led == LED_TX)
        LED_TX_PORT.OUTTGL = LED_TX_PIN_bm;
    else if(led == LED_RX)
        LED_RX_PORT.OUTTGL = LED_RX_PIN_bm;
    else if(led == LED_MOTOR_1)
        LED_MOTOR_1_PORT.OUTTGL = LED_MOTOR_1_PIN_bm;
    else if(led == LED_MOTOR_2)
        LED_MOTOR_2_PORT.OUTTGL = LED_MOTOR_2_PIN_bm;
    else if(led == LED_OVERFLOW)
        LED_OVERFLOW_PORT.OUTTGL = LED_OVERFLOW_PIN_bm;
    else if(led == LED_UNDERFLOW)
        LED_UNDERFLOW_PORT.OUTTGL = LED_UNDERFLOW_PIN_bm;
    else {}
}

//[[[end]]]

volatile led_alert_t led_alert;

void led_update()
{
    if(status.volume_manque > cconfig.volume_manque_max - cconfig.debit_no_action)
        led_toogle(LED_UNDERFLOW);
    else
    {
        if(status.debit5.v2 < status.consigne_adj - cconfig.debit_no_action)
            led_on(LED_UNDERFLOW);
        else
            led_off(LED_UNDERFLOW);
    }
    if(status.volume_manque < -cconfig.volume_manque_max + cconfig.debit_no_action)
        led_toogle(LED_OVERFLOW);
    else
    {
        if(status.debit5.v2 > status.consigne_adj + cconfig.debit_no_action)
            led_on(LED_OVERFLOW);
        else
            led_off(LED_OVERFLOW);
    }
    
    if(led_alert.open)
    {
        led_toogle(LED_MOTOR_2);
        led_alert.open--;
    }
    else if(vanne_internal_status == EV_OPENED)
        led_on(LED_MOTOR_2);
    else
        led_off(LED_MOTOR_2);
    
    if(led_alert.close)
    {
        led_toogle(LED_MOTOR_1);
        led_alert.close--;
    }
    else if(vanne_internal_status == EV_CLOSED)
        led_on(LED_MOTOR_1);
    else
        led_off(LED_MOTOR_1);
    if(led_alert.tx)
    {
        led_toogle(LED_TX);
        led_alert.tx--;
    }
    else
        led_off(LED_TX);
    if(led_alert.rx)
    {
        led_toogle(LED_RX);
        led_alert.rx--;
    }
    else
        led_off(LED_RX);
}
