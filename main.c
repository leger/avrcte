#include "common.h"

int main(void)
{
    init_osc();
    PMIC.CTRL |= PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    sei();

    debug_init();
    status_init();
    config_init();
    vanne_init();
    debit_init();
    led_init();
    main_timer_init();

    uint8_t k;
    for(k=0; k<BID; k++)
        _delay_ms(2500);
    
    bus_init();

    while(1)
    {
        do_orders();
        _delay_ms(10);
    }
}
