#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.ds18b20(
        prefix="thermometer",
        pindesc=conf.SENSOR_T,
        header=False,
    )
)
]]]*/
uint8_t thermometer_reset()
{
    THERMOMETER_port.DIRSET = THERMOMETER_pin;
    THERMOMETER_port.OUTCLR = THERMOMETER_pin;
	_delay_us(480); // 480 min (480)

    THERMOMETER_port.DIRCLR = THERMOMETER_pin;
	_delay_us(60); // 60-255 (60)

    uint8_t ret = THERMOMETER_port.IN & THERMOMETER_pin; // 0: ok
	_delay_us(420);  // (420)

	return ret;
}

void thermometer_writebit(uint8_t bit)
{
    THERMOMETER_port.DIRSET = THERMOMETER_pin;
    THERMOMETER_port.OUTCLR = THERMOMETER_pin;
	_delay_us(1);

	// write 1: release the line
	if(bit)
        THERMOMETER_port.DIRCLR = THERMOMETER_pin;

	_delay_us(60); // 59 min
    THERMOMETER_port.DIRCLR = THERMOMETER_pin;
	_delay_us(10);
}

uint8_t thermometer_readbit(void)
{
	uint8_t ret=0;

    THERMOMETER_port.DIRSET = THERMOMETER_pin;
    THERMOMETER_port.OUTCLR = THERMOMETER_pin;
	_delay_us(1);

    THERMOMETER_port.DIRCLR = THERMOMETER_pin;
	_delay_us(13);

    if(THERMOMETER_port.IN & THERMOMETER_pin)
		ret=1;
	_delay_us(60);
	return ret;
}

void thermometer_writebyte(uint8_t byte)
{
	uint8_t i;
    for(i=0;i<8;i++)
    {
        thermometer_writebit( byte & 0x01);
        byte >>= 1;
    }
}

uint8_t thermometer_readbyte(void)
{
    uint8_t ret=0;
    uint8_t i;
    for(i=0;i<8;i++)
    {
        ret >>= 1;
        ret |= (thermometer_readbit()<<7);
    }
	return ret;
}

int8_t thermometer_gettemp(int16_t* temp, uint16_t timeout_ms)
{
    uint8_t buf[9];
	
    if(thermometer_reset())
        return 3;
	thermometer_writebyte(THERMOMETER_CMD_SKIPROM);
	thermometer_writebyte(THERMOMETER_CMD_CONVERTTEMP);

    int16_t delay=0;
	while(!thermometer_readbit())
    {
        if(delay>timeout_ms)
            return(2); // timeout
        _delay_ms(1);
        delay++;
    }

	if(thermometer_reset())
        return(4);
	thermometer_writebyte(THERMOMETER_CMD_SKIPROM);
	thermometer_writebyte(THERMOMETER_CMD_RSCRATCHPAD);

    uint8_t i;
    for(i=0;i<9;i++)
        buf[i] = thermometer_readbyte();

    uint8_t crc=0x00;
    for(i=0;i<8;i++)
        crc = _crc_ibutton_update(crc,buf[i]);
    
    if(crc == buf[8])
    {
	    *temp = ( (int16_t)(((uint16_t)(buf[1]))<<8) | buf[0] );
        return(0); // ok
    }
    
    return(1);
}
///[[[end]]]
