#include "orders.h"

orders_t orders;

void do_orders()
{
    if(orders.open_ev_step)
    {
        vanne_step_open(orders.open_ev_step_value,1);
        orders.open_ev_step = 0;
        status.force_closed = 0;
    }
    if(orders.close_ev_step)
    {
        vanne_step_close(orders.close_ev_step_value,1);
        orders.close_ev_step = 0;
    }
    if(orders.open_ev)
    {
        vanne_open();
        orders.open_ev=0;
        status.force_closed = 0;
    }
    if(orders.close_ev)
    {
        vanne_close();
        orders.close_ev=0;
    }
    if(orders.force_close_ev)
    {
        if(!status.force_closed)
        {
            vanne_force_close();
            status.force_closed = 1;
        }
        orders.force_close_ev = 0;
    }
    if(orders.vidange_ev)
    {
        vanne_vidange();
        orders.vidange_ev=0;
        status.force_closed = 0;
    }
    if(orders.write_config)
    {
        orders.write_config=0;
        config_write();
    }
    if(orders.get_T)
    {
        orders.get_T=0;

        int16_t temp;
        uint8_t status_thermo;
        uint8_t tries=0;
        while(1)
        {
            status_thermo = thermometer_gettemp(&temp,1000);
            debug_printf("get temp, status = %u\n", status_thermo);
            if(status_thermo==0)
                break;
            tries++;
            if(tries>=4)
                break;
        }
        if(status_thermo > 0)
        {
            if(status.fail_get_T >= THERMO_FAIL_NUMBER)
                status.fail |= STATUS_FAIL_T;
            else
                status.fail_get_T++;
        }
        else
        {
            status.fail_get_T = 0;
            status.fail &= ~STATUS_FAIL_T;
            status.temperature_insta = temp;
            if(!median_filter_thermometer_filled)
            {
                median_filter_thermometer_fill(temp);
                status.temperature.v1 = status.temperature.v2 = .0625*temp;
            }
        }
        if(median_filter_thermometer_filled)
        {
            median_filter_thermometer_update(status.temperature_insta);
            float Tinstamf = .0625*median_filter_thermometer_read();
            update_filtre2(&(status.temperature), THERMO_FACTOR_FILTRE, Tinstamf);

            float consigne_debit;

            if(cconfig.mode == MODE_AUTO)
            {
                float temperature_diff = status.temperature.v2 - cconfig.consigne_temperature;
                status.accu_integrateur += temperature_diff * 5 / cconfig.ti;
                float max_integrateur = cconfig.debit_max/cconfig.Kp - temperature_diff;
                if(max_integrateur<0)
                    max_integrateur = 0;
                if(status.accu_integrateur>max_integrateur)
                    status.accu_integrateur = max_integrateur;
                if(status.accu_integrateur<0)
                    status.accu_integrateur = 0;
                consigne_debit = cconfig.Kp * (temperature_diff + status.accu_integrateur);
                if(consigne_debit>cconfig.debit_max)
                    consigne_debit = cconfig.debit_max;
                if(consigne_debit < 0)
                {
                    status.consigne_debit = 0;
                    status.volume_manque = 0;
                }
                else
                    status.consigne_debit = consigne_debit;
            }
            else if(cconfig.mode == MODE_POST_REFROI)
            {
                if(status.temperature.v2>cconfig.post_refroi_T2)
                    consigne_debit = cconfig.post_refroi_dmax;
                else if(status.temperature.v2<cconfig.post_refroi_T1)
                    consigne_debit = 0;
                else
                    consigne_debit = (status.temperature.v2-cconfig.post_refroi_T1)/(cconfig.post_refroi_T2-cconfig.post_refroi_T1)*cconfig.post_refroi_dmax;
                if (status.temperature_cold_water > cconfig.post_refroi_cold_water_T2)
                    consigne_debit = 0;
                else if(status.temperature_cold_water > cconfig.post_refroi_cold_water_T1)
                    consigne_debit *= (cconfig.post_refroi_cold_water_T2-status.temperature_cold_water)/(cconfig.post_refroi_cold_water_T2-cconfig.post_refroi_cold_water_T1);
                if (status.temperature_hot_water > cconfig.post_refroi_hot_water_T2)
                    consigne_debit = 0;
                else if(status.temperature_hot_water > cconfig.post_refroi_hot_water_T1)
                    consigne_debit *= (cconfig.post_refroi_hot_water_T2-status.temperature_hot_water)/(cconfig.post_refroi_hot_water_T2-cconfig.post_refroi_hot_water_T1);
                if(consigne_debit < 0)
                    status.consigne_debit = 0;
                else
                    status.consigne_debit = consigne_debit;
            }
        }
    }
}
