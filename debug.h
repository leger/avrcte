#include "common.h"

/*[[[cog
import conf
import avrxmegastuff as ams
]]]*/
//[[[end]]]


/*[[[cog
cog.out(
    ams.uart(
        prefix="debug",
        **conf.DEBUG,
        header=True,
    )
)
]]]*/
#ifndef H_UART_debug
#define H_UART_debug 1

#define DEBUG_port PORTC
#define DEBUG_txpin PIN7_bm
#define DEBUG_rxlen 64
#define DEBUG_txlen 64
#define DEBUG_baudrate 9600
#define DEBUG_chsize 8
#define DEBUG_rxtrigger '\n'
#define DEBUG_rxen false
#define DEBUG_txen true
#define DEBUG_USART USARTC1
#define DEBUG_DRE_vect USARTC1_DRE_vect
#define DEBUG_RXC_vect USARTC1_RXC_vect

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>

void debug_init();

void debug_putchar(uint8_t ch);
void debug_putzchar(const char* zchar);
void debug_putfloat(const float f, uint8_t decsize);
void debug_printf(const char *fmt, ...);
#endif
///[[[end]]]
