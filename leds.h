#include "common.h"

#ifndef H_LEDS
#define H_LEDS 1

/*[[[cog
import conf
import avrxmegastuff

for led_name, led_pd in conf.LED.items():
    cog.out(f"#define LED_{led_name}_PORT {led_pd.port}\n")
    cog.out(f"#define LED_{led_name}_PIN_bm {led_pd.pin_bm}\n")
]]] */
#define LED_STATUS_PORT PORTF
#define LED_STATUS_PIN_bm PIN0_bm
#define LED_TX_PORT PORTF
#define LED_TX_PIN_bm PIN1_bm
#define LED_RX_PORT PORTF
#define LED_RX_PIN_bm PIN2_bm
#define LED_MOTOR_1_PORT PORTF
#define LED_MOTOR_1_PIN_bm PIN3_bm
#define LED_MOTOR_2_PORT PORTF
#define LED_MOTOR_2_PIN_bm PIN4_bm
#define LED_OVERFLOW_PORT PORTF
#define LED_OVERFLOW_PIN_bm PIN5_bm
#define LED_UNDERFLOW_PORT PORTF
#define LED_UNDERFLOW_PIN_bm PIN6_bm
// [[[end]]]

/*[[[cog
cog.out("typedef enum leds_enum\n{\n")
for led_name in conf.LED.keys():
    cog.out(f"    LED_{led_name},\n")
cog.out("} led_t;\n")
]]] */
typedef enum leds_enum
{
    LED_STATUS,
    LED_TX,
    LED_RX,
    LED_MOTOR_1,
    LED_MOTOR_2,
    LED_OVERFLOW,
    LED_UNDERFLOW,
} led_t;
// [[[end]]]

void led_init();

/*[[[cog
for act in ("on", "off", "toogle"):
    cog.out(f"void led_{act}(led_t led);\n")
]]] */
void led_on(led_t led);
void led_off(led_t led);
void led_toogle(led_t led);
// [[[end]]]

typedef struct led_alert_struct
{
    uint8_t open;
    uint8_t close;
    uint8_t tx;
    uint8_t rx;
} led_alert_t;

extern volatile led_alert_t led_alert;

void led_update();

#endif
