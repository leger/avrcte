#include "common.h"

#ifndef H_ORDERS
#define H_ORDERS 1

typedef struct orders_struct
{
    uint8_t open_ev_step;
    uint16_t open_ev_step_value;
    uint8_t close_ev_step;
    uint16_t close_ev_step_value;
    uint8_t open_ev;
    uint8_t close_ev;
    uint8_t force_close_ev;
    uint8_t vidange_ev;
    uint8_t get_T;
    uint8_t write_config;
} orders_t;

extern orders_t orders;

void do_orders();

/*[[[cog
import conf
cog.out(f"#define THERMO_FACTOR_FILTRE {conf.THERMO_FACTOR_FILTRE:e}\n")
cog.out(f"#define THERMO_FAIL_NUMBER {conf.THERMO_FAIL_NUMBER:d}\n")
]]] */
#define THERMO_FACTOR_FILTRE 7.995559e-02
#define THERMO_FAIL_NUMBER 180
//[[[end]]]

#endif
