#include "messages.h"

uint8_t get_packet(char * z, uint8_t pid)
{
    uint8_to_2char(pid,z);
    if(pid == 0x01)
    {
        uint8_to_2char(status.fail, z+2);
        return 4;
    }
    if(pid == 0x02)
    {
        int16_t itemp = (status.temperature.v2 * 100 + .5);
        uint16_to_4char((uint16_t)itemp, z+2);
        return 6;
    }
    if(pid == 0x03)
    {
        uint16_to_4char((uint16_t)status.temperature_insta, z+2);
        return 6;
    }
    if(pid == 0x04)
    {
        uint16_to_4char((uint16_t)(status.debit60.v2 * 100 + .5), z+2);
        return 6;
    }
    if(pid == 0x05)
    {
        uint16_to_4char((uint16_t)(status.debit5.v2 * 100 + .5), z+2);
        return 6;
    }
    if(pid == 0x06)
    {
        uint16_to_4char((uint16_t)(status.volume_manque * 100 + .5), z+2);
        return 6;
    }
    if(pid == 0x07)
    {
        uint16_to_4char((uint16_t)(status.consigne_debit * 100 + .5), z+2);
        return 6;
    }
    if(pid == 0x08)
    {
        uint16_to_4char((uint16_t)(status.consigne_adj * 100 + .5), z+2);
        return 6;
    }
    if(pid == 0x09)
    {
        uint16_to_4char((uint16_t)(status.accu_integrateur * 1000 + .5), z+2);
        return 6;
    }
    if(pid == 0x0a)
    {
        int16_t icwtemp = (status.temperature_cold_water * 100 + .5);
        uint16_to_4char((uint16_t)icwtemp, z+2);
        return 6;
    }
    if(pid == 0x0b)
    {
        int16_t ichtemp = (status.temperature_hot_water * 100 + .5);
        uint16_to_4char((uint16_t)ichtemp, z+2);
        return 6;
    }

    if(pid == 0x80)
    {
        uint8_to_2char((uint8_t)config.mode, z+2);
        return 4;
    }
    if(pid == 0x81)
    {
        uint16_to_4char(config.vol_cuve, z+2);
        return 6;
    }
    if(pid == 0x82)
    {
        uint16_to_4char(config.Kp, z+2);
        return 6;
    }
    if(pid == 0x83)
    {
        uint16_to_4char(config.ti, z+2);
        return 6;
    }
    if(pid == 0x84)
    {
        uint16_to_4char(config.consigne_temperature, z+2);
        return 6;
    }
    if(pid == 0x85)
    {
        uint16_to_4char(config.debit_max, z+2);
        return 6;
    }
    if(pid == 0x86)
    {
        uint8_to_2char(config.cst_ev, z+2);
        return 4;
    }
    if(pid == 0x88)
    {
        uint8_to_2char(config.minimum_step, z+2);
        return 4;
    }
    if(pid == 0x89)
    {
        uint16_to_4char(config.volume_manque_max, z+2);
        return 6;
    }
    if(pid == 0x8a)
    {
        uint8_to_2char(config.m_rattrapage, z+2);
        return 4;
    }
    if(pid == 0x8b)
    {
        uint16_to_4char(config.consigne_debit, z+2);
        return 6;
    }
    if(pid == 0x8c)
    {
        uint16_to_4char(config.debit_min1, z+2);
        return 6;
    }
    if(pid == 0x8d)
    {
        uint16_to_4char(config.debit_min2, z+2);
        return 6;
    }
    if(pid == 0x8e)
    {
        uint16_to_4char(config.post_refroi_T1, z+2);
        return 6;
    }
    if(pid == 0x8f)
    {
        uint16_to_4char(config.post_refroi_T2, z+2);
        return 6;
    }
    if(pid == 0x90)
    {
        uint16_to_4char(config.post_refroi_cold_water_T1, z+2);
        return 6;
    }
    if(pid == 0x91)
    {
        uint16_to_4char(config.post_refroi_cold_water_T2, z+2);
        return 6;
    }
    if(pid == 0x92)
    {
        uint16_to_4char(config.post_refroi_hot_water_T1, z+2);
        return 6;
    }
    if(pid == 0x93)
    {
        uint16_to_4char(config.post_refroi_hot_water_T2, z+2);
        return 6;
    }
    if(pid == 0x94)
    {
        uint16_to_4char(config.post_refroi_dmax, z+2);
        return 6;
    }
    if(pid == 0xb1)
    {
        uint8_to_2char(COMMITID_MAJOR, z+2);
        uint8_to_2char(COMMITID_MINOR, z+4);
        uint8_to_2char(COMMITID_NBCOMMITS, z+6);
        uint32_to_8char(COMMITID_DIRTYHASH, z+8);
        return 16;
    }
    return 0;
}


void message_send()
{
    char msg[384];
    msg[0] = 'S';
    msg[1] = ',';
    uint8_to_2char(BID, msg+2);
    msg[4] = ',';
    uint8_t msg_len=5;
    uint8_t packet_len;

    /*[[[cog
    lines = """\
    packet_len = get_packet(msg+msg_len, {packet_id});
    msg_len += packet_len;
    
    """
    
    sep = """\
    msg[msg_len] = ',';
    msg_len++;
    """

    out = sep.join(
        lines.format(packet_id=pid)
        for pid in (
            "0x01", "0x02", "0x03", "0x04", "0x05", "0x06", "0x07", "0x08", 
            "0x09", "0x0a", "0x0b",
            "0x80", "0x81", "0x82", "0x83", "0x84", "0x85", "0x86",
            "0x88", "0x89", "0x8a", "0x8b", "0x8c", "0x8d", "0x8e",
            "0x8f", "0x90", "0x91", "0x92", "0x93", "0x94",
            "0xb1")
    )
    cog.out(out)
    ]]]*/
    packet_len = get_packet(msg+msg_len, 0x01);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x02);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x03);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x04);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x05);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x06);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x07);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x08);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x09);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x0a);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x0b);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x80);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x81);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x82);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x83);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x84);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x85);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x86);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x88);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x89);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8a);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8b);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8c);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8d);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8e);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x8f);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x90);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x91);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x92);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x93);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0x94);
    msg_len += packet_len;

    msg[msg_len] = ',';
    msg_len++;
    packet_len = get_packet(msg+msg_len, 0xb1);
    msg_len += packet_len;

    ///[[[end]]]
    
    msg[msg_len] = ':';
    msg_len++;
    
    uint16_t crc = 0xFFFF;
    uint8_t k;
    for(k=0; k<msg_len; k++)
        crc = _crc_ccitt_update(crc, msg[k]);

    uint16_to_4char(crc, msg+msg_len);
    msg_len += 4;

    msg[msg_len]='\n';
    msg_len++;
    msg[msg_len]=0;

    bus_putzchar(msg);
}

void message_get(const char* msg)
{
    uint8_t message_from_master = 0;
    if(msg[1] != ',')
        return;
    if(msg[0] != 'M')
    {
        //ce n'est pas master qui parle
        if(msg[0] != 'S' || msg[2] != BID_WATER_C0 || msg[3] != BID_WATER_C1)
            return; // ce n'est pas WATER qui parle, et à part lui, je n'écoute
                    // que le master
    }
    else
    {
        message_from_master = 1;
        if(msg[2] != BID_C0 || msg[3] != BID_C1)
            return; // ce n'est pas à moi que master parle
    }
    if(msg[4] != ',' && msg[4] != ':')
        return; //message mal formaté
    uint16_t crc = 0xFFFF;
    uint8_t k;
    uint8_t msg_len=5;
    for(k=0; k<255; k++)
    {
        crc = _crc_ccitt_update(crc, msg[k]);
        if(msg[k] == ':')
        {
            msg_len = k+1;
            break;
        }
    }
    if(msg[msg_len-1] != ':')
        return; // message mal formaté
    char zcrc[4];
    uint16_to_4char(crc, zcrc);
    if(zcrc[0] != msg[msg_len] || zcrc[1] != msg[msg_len+1] || zcrc[2] != msg[msg_len+2] || zcrc[3] != msg[msg_len+3])
        return; // invalid message

    if(message_from_master)
    {
        if(msg_len>5)
        {
            config_t new_config;
            memcpy(&new_config, &config, sizeof(config_t));
            uint8_t pos = 5;
            while(pos<msg_len)
            {
                for(k=0; pos+k<msg_len; k++)
                    if(msg[pos+k] == ',' || msg[pos+k] == ':')
                        break;
                readed_packet(msg+pos, k, &new_config);
                pos += k+1;
                if(msg[pos-1] == ':')
                    break;
            }
            if(memcmp(&new_config, &config, sizeof(config_t)))
            {
                cconfig_t new_cconfig;
                cconfig_update(&new_config, &new_cconfig);
                if(config_valid(&new_cconfig))
                    config_apply_new_config(&new_config);
            }
        }

        // we can reply
        message_send();
    }
    else
    {
        if(msg_len>5)
        {
            uint8_t pos = 5;
            while(pos<msg_len)
            {
                for(k=0; pos+k<msg_len; k++)
                    if(msg[pos+k] == ',' || msg[pos+k] == ':')
                        break;
                update_temp_readed_status_packet(msg+pos, k);
                pos += k+1;
            }
        }
    }
}

void update_temp_readed_status_packet(const char *z, uint8_t len)
{
    if(len<2)
        return;
    uint8_t pid = uint8_from_2char(z);
    if(pid == 0x0a)
    {
        if(len!=6)
            return;
        status.temperature_cold_water = 0.01*uint16_from_4char(z+2);
    }
    if(pid == 0x0b)
    {
        if(len!=6)
            return;
        status.temperature_hot_water = 0.01*uint16_from_4char(z+2);
    }
}


void readed_packet(const char * z, uint8_t len, config_t * nc)
{
    if(len<2)
        return;
    uint8_t pid = uint8_from_2char(z);
    if(pid == 0x80)
    {
        if(len!=4)
            return;
        nc->mode = (mode_t)uint8_from_2char(z+2);
        return;
    }
    if(pid == 0x81)
    {
        if(len!=6)
            return;
        nc->vol_cuve = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x82)
    {
        if(len!=6)
            return;
        nc->Kp = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x83)
    {
        if(len!=6)
            return;
        nc->ti = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x84)
    {
        if(len!=6)
            return;
        nc->consigne_temperature = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x85)
    {
        if(len!=6)
            return;
        nc->debit_max = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x86)
    {
        if(len!=4)
            return;
        nc->cst_ev = uint8_from_2char(z+2);
        return;
    }
    if(pid == 0x88)
    {
        if(len!=4)
            return;
        nc->minimum_step = uint8_from_2char(z+2);
        return;
    }
    if(pid == 0x89)
    {
        if(len!=6)
            return;
        nc->volume_manque_max = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x8a)
    {
        if(len!=4)
            return;
        nc->m_rattrapage = uint8_from_2char(z+2);
        return;
    }
    if(pid == 0x8b)
    {
        if(len!=6)
            return;
        nc->consigne_debit = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x8c)
    {
        if(len!=6)
            return;
        nc->debit_min1 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x8d)
    {
        if(len!=6)
            return;
        nc->debit_min2 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x8e)
    {
        if(len!=6)
            return;
        nc->post_refroi_T1 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x8f)
    {
        if(len!=6)
            return;
        nc->post_refroi_T2 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x90)
    {
        if(len!=6)
            return;
        nc->post_refroi_cold_water_T1 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x91)
    {
        if(len!=6)
            return;
        nc->post_refroi_cold_water_T2 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x92)
    {
        if(len!=6)
            return;
        nc->post_refroi_hot_water_T1 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x93)
    {
        if(len!=6)
            return;
        nc->post_refroi_hot_water_T2 = uint16_from_4char(z+2);
        return;
    }
    if(pid == 0x94)
    {
        if(len!=6)
            return;
        nc->post_refroi_dmax = uint16_from_4char(z+2);
        return;
    }
}
