#include "common.h"

#ifndef H_DEBIT
#define H_DEBIT 1

/*[[[cog
import conf
import avrxmegastuff

cog.out(f"#define FLOW_PORT {conf.FLOW.port}\n")
cog.out(f"#define FLOW_PIN_bm {conf.FLOW.pin_bm}\n")
cog.out(f"#define FLOW_PINCTRL {conf.FLOW.pinctrl}\n")
cog.out(f"#define FLOW_EVSYS_CHMUX {conf.FLOW.evsys_chmux_gc}\n")
cog.out(f"#define FLOW_EVSYS_CHANNELMUX EVSYS.CH{conf.FLOW_EVSYS_CHANNEL}MUX\n")
cog.out(f"#define FLOW_EVSYS_CHANNELCTRL EVSYS.CH{conf.FLOW_EVSYS_CHANNEL}CTRL\n")
cog.out(f"#define FLOW_COUNTER_EVENT TC_CLKSEL_EVCH{conf.FLOW_EVSYS_CHANNEL}_gc\n")
cog.out(f"#define FLOW_COUNTER {conf.FLOW_COUNTER}\n")

cog.out("\n")
cog.out(f"#define FLOW_FACTOR_FILTRE_5 {conf.FLOW_FACTOR_FILTRE_5:e}\n")
cog.out(f"#define FLOW_FACTOR_FILTRE_60 {conf.FLOW_FACTOR_FILTRE_60:e}\n")
]]] */
#define FLOW_PORT PORTE
#define FLOW_PIN_bm PIN2_bm
#define FLOW_PINCTRL PORTE.PIN2CTRL
#define FLOW_EVSYS_CHMUX EVSYS_CHMUX_PORTE_PIN2_gc
#define FLOW_EVSYS_CHANNELMUX EVSYS.CH1MUX
#define FLOW_EVSYS_CHANNELCTRL EVSYS.CH1CTRL
#define FLOW_COUNTER_EVENT TC_CLKSEL_EVCH1_gc
#define FLOW_COUNTER TCD0

#define FLOW_FACTOR_FILTRE_5 1.812692e-01
#define FLOW_FACTOR_FILTRE_60 1.652855e-02
// [[[end]]]

void debit_init();
void debit_consigne_update(float consigne);
void debit_update();
void debit_vanne_update();

#endif
