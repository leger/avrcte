#include "config.h"

config_t config;
cconfig_t cconfig;

void config_write()
{
    uint16_t k;
    for(k=0;k<sizeof(config_t);k++)
        eeprom_write_byte(EEPROM_ADDR_CONFIG+k, *(((uint8_t*)&config)+k));
}

void config_read()
{
    uint16_t k;
    for(k=0;k<sizeof(config_t);k++)
        *(((uint8_t*)&config)+k) = eeprom_read_byte(EEPROM_ADDR_CONFIG+k);
}

void config_init()
{

    config_read();
    cconfig_update(&config, &cconfig);
    if(!config_valid(&cconfig))
    {
        config_default(&config);
        cconfig_update(&config, &cconfig);
    }
    config_mode_change();
}

void cconfig_update(config_t * c, cconfig_t * cc)
{
    cc->mode = c->mode;
    cc->Kp = (0.01 * c->Kp * c->vol_cuve) / 300;
    cc->ti = c->ti;
    cc->consigne_temperature = 0.01 * c->consigne_temperature;
    cc->debit_max = (0.01 * c->debit_max * c->vol_cuve) / 300;
    cc->cst_ev = c->cst_ev;
    cc->debit_min1 = 0.01 * c->debit_min1;
    cc->debit_min2 = 0.01 * c->debit_min2;
    cc->minimum_step = c->minimum_step;
    cc->debit_no_action = cc->minimum_step/cc->cst_ev;
    cc->volume_manque_max = 0.01 * c->volume_manque_max;
    cc->m_rattrapage = c->m_rattrapage;
    cc->consigne_debit = 0.01 * c->consigne_debit;
    cc->post_refroi_T1 = 0.01 * c->post_refroi_T1;
    cc->post_refroi_T2 = 0.01 * c->post_refroi_T2;
    cc->post_refroi_cold_water_T1 = 0.01 * c->post_refroi_cold_water_T1;
    cc->post_refroi_cold_water_T2 = 0.01 * c->post_refroi_cold_water_T2;
    cc->post_refroi_hot_water_T1 = 0.01 * c->post_refroi_hot_water_T1;
    cc->post_refroi_hot_water_T2 = 0.01 * c->post_refroi_hot_water_T2;
    cc->post_refroi_dmax = (0.01 * c->post_refroi_dmax * c->vol_cuve)/300;
}

uint8_t config_valid(cconfig_t * cc)
{
    if(cc->mode<1 || cc->mode>6)
        return 0;
    if(cc->Kp<1 || cc->Kp>100)
        return 0;
    if(cc->ti<60 || cc->ti > 86400)
        return 0;
    if(cc->consigne_temperature < 10 || cc->consigne_temperature > 40)
        return 0;
    if(cc->debit_max < 5 || cc->debit_max > 50)
        return 0;
    if(cc->cst_ev < 1 || cc->cst_ev>100)
        return 0;
    if(cc->debit_min1 < 1 || cc->debit_min1 > 40)
        return 0;
    if(cc->debit_min2 < 1 || cc->debit_min2 > 40)
        return 0;
    if(cc->debit_min1 >= cc->debit_min2)
        return 0;
    if(cc->debit_min2 >= cc->debit_max)
        return 0;
    if(cc->minimum_step < 1 || cc->minimum_step > 150)
        return 0;
    if(cc->volume_manque_max < 1 || cc->volume_manque_max>250)
        return 0;
    if(cc->m_rattrapage < 1 || cc->m_rattrapage > 60)
        return 0;
    if(cc->consigne_debit<0 || cc->consigne_debit>50)
        return 0;
    if(cc->post_refroi_T1 < 7 || cc->post_refroi_T2 > 40 || cc->post_refroi_T2-cc->post_refroi_T1<1)
        return 0;
    if(cc->post_refroi_cold_water_T1 < 1 || cc->post_refroi_cold_water_T2 > 40 || cc->post_refroi_cold_water_T2-cc->post_refroi_cold_water_T1<1)
        return 0;
    if(cc->post_refroi_hot_water_T1 < 1 || cc->post_refroi_hot_water_T2 > 40 || cc->post_refroi_hot_water_T2-cc->post_refroi_hot_water_T1<1)
        return 0;
    if(cc->post_refroi_dmax < 0 || cc->post_refroi_dmax>40)
        return 0;
    return 1;
}

void config_default(config_t* c)
{
    c->mode = (uint8_t)MODE_FORCE_CLOSE;
    c->vol_cuve = 300;
    c->Kp = 32*100;
    c->ti = 3600;
    c->consigne_temperature = 2395;
    c->debit_max = 1500;
    c->cst_ev = 10;
    c->debit_min1 = 200;
    c->debit_min2 = 400;
    c->minimum_step = 10;
    c->volume_manque_max = 7500;
    c->m_rattrapage = 15;
    c->consigne_debit = 0;
    c->post_refroi_T1 = 1200;
    c->post_refroi_T2 = 1400;
    c->post_refroi_cold_water_T1 = 750;
    c->post_refroi_cold_water_T2 = 1050;
    c->post_refroi_hot_water_T1 = 1050;
    c->post_refroi_hot_water_T2 = 1350;
    c->post_refroi_dmax = 1000;
}

void config_apply_new_config(config_t * pnew_config)
{
    cconfig_t new_cconfig;
    cconfig_update(pnew_config, &new_cconfig);
    if(config_valid(&new_cconfig))
    {
        uint8_t mode_change = 0;
        if(pnew_config->mode != config.mode)
            mode_change = 1;
        memcpy(&config, pnew_config, sizeof(config_t));
        orders.write_config = 1;
        cconfig_update(&config, &cconfig);
        if(mode_change)
            config_mode_change();
        if(config.mode == MODE_DEBIT)
            status.consigne_debit = cconfig.consigne_debit;
    }
}

void config_mode_change()
{
    if(config.mode == MODE_FORCE_CLOSE)
    {
        orders.close_ev = 1;
        status.consigne_debit = 0;
        status.volume_manque = 0;
    }
    else if(config.mode == MODE_FORCE_OPEN)
    {
        orders.open_ev = 1;
        status.consigne_debit = 0;
        status.volume_manque = 0;
    }
    else if(config.mode == MODE_FORCE_VIDANGE)
    {
        orders.vidange_ev = 1;
        status.consigne_debit = 0;
        status.volume_manque = 0;
    }
    else if(config.mode == MODE_DEBIT)
    {
        orders.close_ev = 1;
        status.volume_manque = 0;
        status.consigne_debit = cconfig.consigne_debit;
    }
    else if(config.mode == MODE_AUTO)
    {
        orders.close_ev = 1;
        status.volume_manque = 0;
        status.accu_integrateur = 0;
    }
    else if(config.mode == MODE_POST_REFROI)
    {
        orders.close_ev = 1;
        status.volume_manque = 0;
        status.consigne_debit = 0;
    }
}
