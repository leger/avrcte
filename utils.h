#include "common.h"

#ifndef H_UTILS
#define H_UTILS 1

typedef struct filtre2_struct
{
    float v1;
    float v2;
} filtre2_t;

void update_filtre2(filtre2_t* pf, float factor, float value);

#endif

char snip_to_char(uint8_t snip);
void uint8_to_2char(uint16_t nb, char* c);
void uint16_to_4char(uint16_t nb, char* c);
void uint32_to_8char(uint32_t nb, char* c);

uint8_t uint8_from_2char(const char* c);
uint16_t uint16_from_4char(const char* c);
uint32_t uint32_from_8char(const char* c);
